/*
 * This code supports mac spoofing please un-comment the line in
 * the file located in left side (project explorer) with this path
 * arduino/core/core_esp8266_main.cpp in this file change the value of mac[]
 * in user_init.c function line 167 and 168
 * Also wifi access point is created by this code is by default hidden
 * Buffer is also supported about 15 hr
 * Fixed the time compensation in second
 * improved configuration page
 * OTA feature support with update counter
 * hardware version 15 FCU
 */

#define			debug		             1   	// Debug option 1 enable 0 disable
#define			httpPort 				 8080	// data is sent to this port of connected gateway
#define			new_EEP_location	 	 0		// for detecting the fresh ESP chip
#define			EEP_strings_start		 1		// location of starting of string
#define 		counters_start			 201		// location of starting of counters
#define			ss  					 4		// pin for slave select
const char 		*hrdwre_ver 		= 	"20";	// !!!!Be careful, this keyword forms the directory path to search for OTA binary

#define max_oper_regis 32						 // Maximum number of register for SPI communication
uint8_t operation_regis[max_oper_regis] = {0};	 // array of 16 bit unsigned integer

#include <SPI.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <Ticker.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266httpUpdate.h>

#include "html.h"								 // Header file which includes the HTML page

extern "C" {
#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "user_interface.h"
#include "cont.h"
}

String get_version = "";					// To hold the version string

Ticker tick;								// object for creating the timer event
ESP8266WebServer server(80);				// server at this port with IP 192.168.4.1
ESP8266WiFiMulti WiFiMulti;					// To handle multiple client at http OTA
WiFiClient client;							// client object for communication to the server
int16_t second = 0;               	// variable for storing second count
uint32_t response_count = 0;				// Counter for counting the response since powering on
String cli_MAC = WiFi.macAddress();			// Mac address read for this device
uint8_t network_error = 0;					// network error for each reading attempts
uint8_t last_upd_cnt = 0;					// counter that shows the last update attempt
int AP_status = 0;

struct EEP_strings {						// Strings which are being stored in eeprom
  char  Network_SSID[32];					// Name of SSID to connect
  char  Network_password[32];				// password of that network
  char  Network_gateway[32];				// IP address of the server to communicate
  char  AUTH_pass[32];
  char	AP_password[32];
  char firm_ver[5];
  uint8_t last_update;
}EEP_String;

struct others{								// Other counters
  uint32_t reboot;
  uint32_t reconnect;						// Reconnect counter if attempts of connection successes
  uint8_t  comm_delay;					// Sampling of temperature at this rate
  uint32_t network_fail;					// reboot counter when occur due to network fail
}counter;

/*
 * Function that takes structure and stores that into given location
 * Arguments: address where the data has to be stored, data structure pointer and size of that structure
 * Returns: void
 */
void EEPROM_write_obj(int addr,char * ptr,int siz){
  for(int i= addr;i<(siz + addr);i++){
    EEPROM.write(i,*(ptr));
    ptr++;
    EEPROM.commit();
  }
}

/*
 * Function for reading the data from eeprom
 * Arguments: address from the data has to be read, data structure pointer and size of that structure
 * returns void
 */
void EEPROM_read_obj(int addr, char * ptr, int siz){
  for(int i= addr;i<(siz + addr);i++){
    *(ptr) = EEPROM.read(i);
    ptr++;
  }
}

/*
 * Calling this function would restart the hardware from GPIO signal at 12th
 */
void system_restart_hard(void){
	EEPROM.end();
	delay(100);						// wait for buffer empty before restart
	pinMode(16,OUTPUT);
	digitalWrite(16,LOW);
}

/*
 * Every second call back function
 */
void count_sec(void){
	digitalWrite(2,0);							// Make the on board LED on
	struct others *count = &counter;			// Pointer for reading and writing the counters
	bool reset = 0;								// if device has to be reset
#if debug
	Serial.println("second="+String(second));
	Serial.println("Net err="+String(network_error));
#endif
	second++;									// Increase the temperature value
	if(network_error>=20){
		count->network_fail++;				// If gateway is unreachable 10 times, save reboot cause to eeprom and reset
		reset  = 1;
	}
	if(reset){									// Reset the device
		EEPROM_write_obj(counters_start,(char *)count,sizeof(*count));  // Finally write all the values and
		system_restart_hard();											// and reset with GPIO
	}
	if(analogRead((unsigned char)A0)>10){
		second = 0;
		Serial.println("In AP mode only");
		if(!AP_status) {
			AP_status = WiFi.softAP(("FCU_"+cli_MAC).c_str(),"xitanez123!@#"); // Start access point with "ESP_'(mac of device)'" string
		}
	}
	digitalWrite(2,1);							// Off the on board LED
}

/*
 * This function gets called every time when new client is connected to the 192.168.4.1 for sensor configuration
 */
void handleRoot(void) {
#if debug
	for(char i=0;i<server.args();i++){
		Serial.println(server.arg(i));
	}
#endif
	if(server.arg(0)=="zenatix123!@#"){											// If user knows the access password i.e. zenatix123!@#
		struct EEP_strings *net_ptr = &EEP_String;
		if(server.arg(1)!="") strcpy(net_ptr->AP_password,server.arg(1).c_str()); // If Access password is also filled by user
		strcpy(net_ptr->Network_SSID,server.arg(2).c_str());					// Copy SSID
		strcpy(net_ptr->Network_password,server.arg(3).c_str());				// and password
		strcpy(net_ptr->Network_gateway,server.arg(4).c_str());					// and gateway IP
		if(server.arg(5)!="") strcpy(net_ptr->AUTH_pass,server.arg(5).c_str()); // If user wants to change the access password too
		EEPROM_write_obj(EEP_strings_start,(char *)net_ptr,sizeof(*net_ptr));	// write all the objects to the eeprom
	}
	   int size = strlen(EEP_String.Network_password);							// Get the password length of the connected network
	   char pass_to_show[size];													// buffer for storing the password to print in the HTML
	   bool toggle=1;int i=0;													// counter for toggling the space with *
	   for(;i<size;i++){
	       if(toggle)pass_to_show[i] = EEP_String.Network_password[i];			// show the password replaced with * character
	       else pass_to_show[i] = '*';
	       toggle = !toggle;
	   }
	   pass_to_show[i] = 0;

	server.send(200, "text/html",page_str1+cli_MAC+page_str2+WiFi.localIP().toString()+page_str3+wifi_station_get_rssi()  	//And send the html form
		                +page_str3_5+hrdwre_ver+page_str3_6+EEP_String.firm_ver
						+page_str4+String(counter.reboot)+page_str5+String(counter.reconnect)+page_str6
		                +String(response_count)+page_str7+String(counter.comm_delay)+page_str8+EEP_String.AUTH_pass
		                +page_str9+EEP_String.AP_password+page_str10+EEP_String.Network_SSID+page_str11+pass_to_show
		                +page_str12+EEP_String.Network_gateway+page_str13+button);
}

/*
 * This function initializes the EEPROM when module gets woke up
 * Arguments: nothing
 * Returns: nothing
 */
void EEPROM_init(void){
	uint8_t new_byte = EEPROM.read(new_EEP_location);  // Look for new EEPROM location to detect if chip is new
	struct EEP_strings *net_ptr = &EEP_String;			// Structures pointers for reading and writing structures values
	struct others *count = &counter;
#if debug
	Serial.println(new_byte);
#endif
	if(new_byte!=0){								// If new ESP chip is detected
#if debug
		Serial.println("Initializing EEPROM first time");
#endif
		strcpy(net_ptr->AP_password,"xitanez123!@#");     // Copy all the default values to the memory
		strcpy(net_ptr->AUTH_pass,"zenatix123");
		strcpy(net_ptr->Network_SSID,"Zenatix_Testing_ESP");
		strcpy(net_ptr->Network_gateway,"192.168.0.50");
		strcpy(net_ptr->Network_password,"qwerty1234");
		strcpy(net_ptr->firm_ver,"2.0");
		count->reboot 			= 0;
		count->reconnect 		= 0;
		count->comm_delay		= 30;
		count->network_fail  	= 0;
		net_ptr->last_update    = 0;
		EEPROM_write_obj(EEP_strings_start,(char *)net_ptr,sizeof(*net_ptr));
		EEPROM_write_obj(counters_start,(char *)count,sizeof(*count));
		EEPROM.write(new_EEP_location,0);
		EEPROM.commit();
	}
	EEPROM_read_obj(EEP_strings_start,(char *)net_ptr,sizeof(*net_ptr)); 			//Read last values
	EEPROM_read_obj(counters_start,(char *)count,sizeof(*count));
	count->reboot ++;																	//Reboot counter gets increase
	EEPROM_write_obj(counters_start,(char *)count,sizeof(*count));
}

/*
 * This function check if reconnect is made and increments the reconnect counter
 * Arguments: Nothing
 * Returns: bool true if connection is made due to re-attempt
 */
//bool check_reconnect(void){
//	if(queue&&client.connect(EEP_String.Network_gateway, 8080)){  // If gateway is present and last time it was not means this time it got connection
//				struct others *count = &counter;
//				count->reconnect ++;											// Increment the reconnect counter
//				EEPROM_write_obj(counters_start,(char *)count,sizeof(*count));  // save it to the eeprom and return
//				return 1;
//			}
//	return 0;
//
//}
bool wifi_net = 0;										// variable that is used to save the last status of network
bool check_reconnect(void){
	if((wifi_net) && (WiFi.status()==WL_CONNECTED)){	// If gateway is present and last time it was not means this time it got connection
		struct others *count = &counter;
#if debug
		Serial.println("Reconnection occurred");
#endif
		counter.reconnect ++;							// Increment the reconnect counter
		EEPROM_write_obj(counters_start,(char *)count,sizeof(*count));  // save it to the eeprom and return
		return 1;
			}
	return 0;
}
/*
 * This function sends the GET request for binary file after loading the bin file it restarts the system for
 * the next startup
 */
void update_ESP_via_OTA(void){
	struct EEP_strings *net_ptr = &EEP_String;
	net_ptr->last_update = 1;
	tick.detach();								// disable the one second timer interrupt
#if debug
	Serial.println("Looking for ota packet");
#endif
	uint16_t time_spent = 0;					// Timeout functionality
	while((time_spent<=60000)&&(WiFiMulti.run() != WL_CONNECTED)){	// check for connection with certain time period
		time_spent++;					// Increase the time at every 1 ms
		delay(1);						// 1 ms delay
	}
	if(time_spent>=60000) {				// If time is spent and couln't get the response
#if debug
	Serial.println("Failed :( restarting");	// Print log
	delay(50);								//// wait for buffer empty before restart
#endif
		system_restart_hard();				// And restart
	}
	else {
#if debug
		Serial.println("/docs/ESP_OTA_binaries/hardware_v_"+String(hrdwre_ver)+"/ESP12_F_v"+String(get_version)+".bin"); //print the path for requesting the binaries
#endif
		HTTPUpdateResult update = ESPhttpUpdate.update(EEP_String.Network_gateway, 8080, "/docs/ESP_OTA_binaries/hardware_v_"+String(hrdwre_ver)
								+"/ESP12_F_v"+String(get_version)+".bin"); //Else download the firmware
#if debug
		Serial.println(update);
#endif
		if(update == HTTP_UPDATE_OK){
		strcpy(net_ptr->firm_ver,(get_version.substring(0,5)).c_str());					// copy the version number
		net_ptr->last_update = 2;														// update counter for plotting the last OTA attempt
		EEPROM_write_obj(EEP_strings_start,(char *)net_ptr,sizeof(*net_ptr));		// store the new version
#if debug
		Serial.println(net_ptr->firm_ver);
		Serial.println(net_ptr->Network_SSID);
		Serial.println(net_ptr->AP_password);
		Serial.println(net_ptr->Network_gateway);
		Serial.println("OTA upgrade success :)");	// And print the log
		Serial.println("Going for reboot now");
#endif
		}
		else {
#if debug
		Serial.println("Failed :( restarting");	// And print the log
#endif
		}
		system_restart_hard();
	}
}

/*
 * This function read the data from the slave and stores into the Operational registers
*/
void read_reg_from_slave(void){
	digitalWrite(ss,LOW);						// Make SS pin LOW for initiate transaction
	SPI.transfer(max_oper_regis/2);				// transfer the LSB of int value
	digitalWrite(ss,HIGH);						// Terminate one session
	delay(1);
	digitalWrite(ss,LOW);						// Make SS pin LOW for initiate transaction
	SPI.transfer(0xA5);							// transfer the LSB of int value
	digitalWrite(ss,HIGH);						// Terminate one session
	delay(1);										// wait for slave to be ready
	for(int i=0;i<max_oper_regis;i++){			// read all the values into the register locally
		delay(1);
		digitalWrite(ss,LOW);						// Make SS pin LOW for initiate transaction
		operation_regis[i] = SPI.transfer(0xff);				// transfer the LSB of int value
		digitalWrite(ss,HIGH);						// Terminate one session
	}
}

/*
 * This function writes the data to the slave from the operational register
 */
void write_reg_to_slave(void){
	digitalWrite(ss,LOW);						// Make SS pin LOW for initiate transaction
	SPI.transfer(max_oper_regis/2);				// transfer the LSB of int value
	digitalWrite(ss,HIGH);						// Terminate one session
	delay(1);
	digitalWrite(ss,LOW);						// Make SS pin LOW for initiate transaction
	SPI.transfer(0xB5);				// transfer the LSB of int value
	digitalWrite(ss,HIGH);						// Terminate one session
	for(int i=((max_oper_regis/2));i<max_oper_regis;i++){	// send the data to the slave from the operational registers
			delay(1);
			digitalWrite(ss,LOW);						// Make SS pin LOW for initiate transaction
			SPI.transfer(operation_regis[i]);				// transfer the LSB of int value
			digitalWrite(ss,HIGH);						// Terminate one session
	}
}

/*
 * This function reads the registers from the server response and stores into the
 */
void read_reg_from_server(String response_str){
	char *start,*end;
	for(int i=(max_oper_regis/2);i<max_oper_regis;i++){
	String start_ind = "&R";									// Starting address will be the "&R" key
	start_ind += String(i);										// Order of the register indexed to the Operational registers
	start_ind += "=";											// Also response contains the "=" character
	String end_ind;												// for the strings end token
	if(i!=(max_oper_regis-1)){									// if read value is not of the last register
	end_ind = "&R";												// make the end between consecutive locations
	end_ind += String(i+1)+"=";
	}
	else end_ind += "&\"";										// otherwise &" will be the end token
	uint16_t ret_int=0;											// Initialize with blank
	start = strstr(response_str.c_str(),start_ind.c_str())+5;	// find the version starting char pointer
	end	=	strstr(response_str.c_str(),end_ind.c_str());		// find the version ending char pointer
	for(;start<end;start++){									// store the response between them in string
		ret_int = (ret_int*10)+((int)(*start)-48);				// convert it to the 16 bit integer before storing
		}
	if(ret_int!=0)												// If SPI is receiving valid values
	operation_regis[i] = ret_int;								// then store it to the Operation register
	}
}

/*
 * This function writes the registers values to the client put request in string format
 */
void write_reg_to_server(void){
	for(int i=0;i<(max_oper_regis);i++){
		client.print("&R"+String(i)+"="+String(operation_regis[i]));
	}
}

/*
 * Read the response from server after data send with put request
 * Returns: values received by the server response
 * Arguments: Nothing
 */
uint16_t get_response(void){
	char ch = 0;							// dummy character for storing received byte from server
	char *start,*end;						// pointer variables to store the
	String extr_val ="";					// Dummy string to store whole response line
	uint16_t ret_sample = 0;				// variable to store the returned value from server
	while (client.connected()) {			// while this device is connected
	    while (client.available()) {		// while gateway is available
	    ch = client.read();					// read one character
	    extr_val += ch;						// store one by one in whole dummy string
	    }
	  }
#if debug
	Serial.println(extr_val);
#endif
	read_reg_from_server(extr_val);
	start = strstr(extr_val.c_str(),"<")+1;	// Starting address of response delay received from server
	end	=	strstr(extr_val.c_str(),">&frmwre_v=");		// ending address of response delay received from server
	for(;start<end;start++){					// from starting index to ending index
			ret_sample = (ret_sample*10)+((int)(*start)-48);	// store the returned value in integer format after
	}
#if debug
	Serial.println(ret_sample);
#endif
	get_version="";						//Initialize with blank
	start = strstr(extr_val.c_str(),"&frmwre_v=")+10;// find the version starting char pointer
	end	=	strstr(extr_val.c_str(),"&");		// find the version ending char pointer
	for(;start<end;start++){				// store the response between them in string
		get_version+=*start;
	}
#if debug
	Serial.println(get_version);			// Print for debug mode
	Serial.println(String(EEP_String.firm_ver).substring(0,5));
#endif
	if((get_version != "") && (get_version != String(EEP_String.firm_ver).substring(0,5))){		// If version number received is not blank and equal
		update_ESP_via_OTA();						// then call the OTA function to update with new firmware
	}
	return ret_sample;
}

/*
 * This function is called after every sampling interval for either putting data into the buffer or send it directly to gateway
 */
void put_request(void){
	digitalWrite(2,0);
	if(EEP_String.last_update) last_upd_cnt++;
	const String put_req = "PUT /data/Temperature/?mac=" ;                          // else this mean device is connected to the gateway
	  client.print(put_req+String(cli_MAC) + "&Firmware_ver=" + String(EEP_String.firm_ver).substring(0,5) +"&Hardware_ver="+hrdwre_ver
			 +"&reboot=" + String(counter.reboot)+"&reconnect_boot="+String(counter.network_fail)+ "&reconnect="+String(counter.reconnect)
			 +"&RSSI="+wifi_station_get_rssi()+"&SSID="+String(EEP_String.Network_SSID)+"&last_updt="+String(EEP_String.last_update)+"&delay="
			 +String(counter.comm_delay)+"&temperature=01010111");
	  write_reg_to_server();
	  client.println(" HTTP/1.1");										// HTTP version header
	  client.println("Host: " + (String)EEP_String.Network_gateway);	// IP of the gateway
	  client.println("User-Agent: BSK/1.0");
	  client.println("Connection: close");
	  client.println("Content-Type: application/x-www-form-urlencoded");
	  client.println();
#if debug
	  Serial.println("Put request");
#endif
	  uint16_t get_delay = get_response();					//Store the delay received from the response
	  if ((!isnan(get_delay)) && get_delay) response_count++;
	  if((!isnan(get_delay)) && get_delay && (get_delay!=counter.comm_delay)){					// If received delay is integer and non zero
		  struct others *count = &counter;
		  count->comm_delay = get_delay;					// Get the value in ram
		  EEPROM_write_obj(counters_start,(char *)count,sizeof(*count)); // store it into the eeprom
	  }
	digitalWrite(2,1);
}

/*
 * This function gets called once after powering on
 */
void setup() {
	pinMode(ss,OUTPUT);
	digitalWrite(ss,1);
	pinMode(2,OUTPUT);				// Indication LED
	pinMode(15,OUTPUT);
	digitalWrite(15,0);
	digitalWrite(2,LOW);			// Make LED ON
	delay(5000); 					// wait for 5 second while power becomes stable
	system_update_cpu_freq(160);	// CPU frequency 160 MHz
	WiFi.mode(WIFI_AP_STA);			// Set for Station and Access point both
	EEPROM.begin(4000);				// allocate 4000 byte in eeprom maximum is 4096
	tick.attach(1,count_sec);		// attach the function count_sec() for every one second
	EEPROM_init();					// EEPROM initialize
#if debug
	Serial.begin(115200);			// begin the serial at 115200 baud rate
	Serial.println();
	Serial.println("Firmware Version - "+String(EEP_String.firm_ver));	// Print serially, the version of firmware
	Serial.println(EEP_String.AP_password);
#endif
	WiFi.softAP(("FCU_"+cli_MAC).c_str(),"xitanez123!@#"); // Start access point with "ESP_'(mac of device)'" string
	server.on("/", handleRoot);									// set the http server at root
	server.begin();												// start that server
	WiFi.begin(EEP_String.Network_SSID, EEP_String.Network_password); //connect to the gateway
#if debug											// Print some of the things for debugging
	Serial.println("MAC "+cli_MAC);					// Print some parameter for serially debugging
	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
	Serial.println("SSID "+String(EEP_String.Network_SSID));
	Serial.println("Password "+String(EEP_String.Network_password));
	Serial.println("Gateway IP "+String(EEP_String.Network_gateway));
	Serial.println("AP Pass "+String(EEP_String.AP_password));
	Serial.println("AUTH pass "+String(EEP_String.AUTH_pass));

	Serial.println("Net fail "+String(counter.network_fail));
	Serial.println("Reboot "+String(counter.reboot));
	Serial.println("Reconnect "+String(counter.reconnect));
	Serial.println("loop delay"+String(counter.comm_delay));
#endif
	WiFiMulti.addAP(EEP_String.Network_SSID, EEP_String.Network_password); // Add connected access point for multiple operation
	SPI.begin();
#if debug
	Serial.println("Started SPI");
#endif
	SPI.setClockDivider(SPI_CLOCK_DIV128);							// Clock division by 128 to make it reliable communication
	digitalWrite(2,HIGH);
}

/*
 * Function that updates the firmware version in ESP8266
 */

void serial_print_registers(void){
	Serial.println("Printing Registers");
	for(int i = 0;i<max_oper_regis;i++){
		Serial.println("Index["+String(i)+"]="+String(operation_regis[i]));
	}
}

void loop() {
	if((second-counter.comm_delay)>=0){				// If sampling time is elapsed
		check_reconnect();								// check reconnect
		wifi_net = 0;										// initialize the flag with zero
		read_reg_from_slave();
		if (!client.connect(EEP_String.Network_gateway, 8080)){		// check if client is available if not then
			if(!WiFi.isConnected()){
				WiFi.disconnect();										// terminate current process
				WiFi.begin(EEP_String.Network_SSID,EEP_String.Network_password);	// and start the connection from begin
#if debug
				Serial.println("gateway not found");	// print this line and
				Serial.println(WiFi.status());			// this error
#endif
				wifi_net = 1;								// raise the flag that device is not connected
				network_error++;						// increase this counter means
			}
		else {
#if debug
				Serial.println("Host Not found");				// print this line and
#endif
				WiFi.disconnect();											// terminate current process
				WiFi.begin(EEP_String.Network_SSID,EEP_String.Network_password);	// and try to connect from begin
				network_error = 0;								// Do not raise the network error because wifi network is connected
			}
		}
		else {
			check_reconnect();								// check reconnect
			wifi_net = 0;
#if debug
			Serial.println("Got IP "+WiFi.localIP().toString());
#endif
			network_error = 0;						// else device is connected to gateway so network error would become zero
			put_request();								// call the put request function for sending or storing the data into buffer
			write_reg_to_slave();
		}

		second = 0; //For the compensation of the error in count second
#if debug
		serial_print_registers();
#endif
	}
	if (last_upd_cnt>9){
		struct EEP_strings *net_ptr = &EEP_String;
		net_ptr->last_update = 0;
		EEPROM_write_obj(EEP_strings_start,(char *)net_ptr,sizeof(*net_ptr)); 			//Read last values
	}
	server.handleClient();					// handle the client at server 192.168.4.1
}
