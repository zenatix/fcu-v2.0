################################################################################
# Automatically-generated file. Do not edit!
################################################################################

INO_SRCS := 
ASM_SRCS := 
O_UPPER_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
PDE_SRCS := 
CC_SRCS := 
AR_SRCS := 
C_SRCS := 
C_UPPER_DEPS := 
PDE_DEPS := 
C_DEPS := 
AR := 
CC_DEPS := 
AR_OBJ := 
C++_DEPS := 
LINK_OBJ := 
CXX_DEPS := 
ASM_DEPS := 
HEX := 
INO_DEPS := 
SIZEDUMMY := 
S_UPPER_DEPS := 
ELF := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
. \
Libraries/ArduinoOTA \
Libraries/EEPROM \
Libraries/ESP8266HTTPClient/src \
Libraries/ESP8266HTTPUpdateServer/src \
Libraries/ESP8266WebServer/src \
Libraries/ESP8266WiFi/src \
Libraries/ESP8266httpUpdate/src \
Libraries/ESP8266mDNS \
Libraries/Ticker \
arduino/core \
arduino/core/libb64 \
arduino/core/spiffs \
arduino/core/umm_malloc \
core/core \
core/core/libb64 \
core/core/spiffs \
core/core/umm_malloc \
libraries/DallasTemperature \
libraries/DallasTemperature/examples/Alarm \
libraries/DallasTemperature/examples/AlarmHandler \
libraries/DallasTemperature/examples/Multibus_simple \
libraries/DallasTemperature/examples/Multiple \
libraries/DallasTemperature/examples/Simple \
libraries/DallasTemperature/examples/Single \
libraries/DallasTemperature/examples/Tester \
libraries/DallasTemperature/examples/TwoPin_DS18B20 \
libraries/DallasTemperature/examples/WaitForConversion \
libraries/DallasTemperature/examples/WaitForConversion2 \
libraries/DallasTemperature/examples/oneWireSearch \
libraries/DallasTemperature/examples/setUserData \
libraries/OneWire \
libraries/OneWire/examples/DS18x20_Temperature \
libraries/OneWire/examples/DS2408_Switch \
libraries/OneWire/examples/DS250x_PROM \
libraries/SPI \

